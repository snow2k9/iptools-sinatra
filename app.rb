require 'sinatra/base'
require 'sinatra/json'

# ******************************************
#
# Renders request.ip in diffrent formats.
# / => html
# /text => plain text
# /json => { ipv4: '255.255.255.255' }
#
# HTML uses milligram.io css for some styles
#
# ******************************************
class IPAddress < Sinatra::Base
  before do
    @prop = Property.new(request)
  end

  get '/' do
    erb :index
  end

  get '/json' do
    json @prop
  end

  get '/text' do
    content_type 'text/plain'
    @prop.to_text
  end
end

require File.expand_path('models/property.rb', File.dirname(__FILE__))
