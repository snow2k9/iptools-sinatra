# Property
class Property
  attr_accessor :ip, :rdns

  def initialize(args)
    self.ip   = args.has_header?('HTTP_X_FORWARDED_FOR') ? args.get_header('HTTP_X_FORWARDED_FOR') : args.ip
    self.rdns = 'localhost'
  end

  def as_json(_options = {})
    {
      ip: @ip,
      rdns: @rdns
    }
  end

  def to_json(*options)
    as_json(*options).to_json(*options)
  end

  def to_text
    ['IP: ' + @ip.to_s, 'rDNS: ' + @rdns.to_s].join("\r\n")
  end

  def to_html
    to_text.gsub(/\r\n/, '<br />')
  end
end
